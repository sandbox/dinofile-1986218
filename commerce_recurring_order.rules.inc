<?php

/**
 * Rules integration for recurring triggers.
 */

/**
 * Implements hook_rules_condition_info().
 */
function commerce_recurring_order_rules_condition_info() {
  $conditions = array();
  
  $conditions['commerce_recurring_order_rules_order_is_recurring'] = array(
    'label' => t('Order marked as recurring'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
        'description' => t('The order in question.'),
      ),
    ),
    'group' => t('Commerce Order'),
  );
  return $conditions;
}

/**
 * Implements hook_rules_action_info().
 */
function commerce_recurring_order_rules_action_info() {
  $actions = array();

  $actions['commerce_recurring_order_update_master'] = array(
    'label' => t('Updates next recurring due date in master orders'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Master order'),
      ),
    ),
    'group' => t('Commerce Recurring Order'),
  );
  
  $actions['commerce_recurring_order_initialise_recurring'] = array(
    'label' => t('Mark an order as a recurring master'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
        'description' => t('The master order'),
      ),
    ),
    'group' => t('Commerce Recurring Order'),
  );

  return $actions;
}

/**
 * Rules action: mark an order as a recurring master
 *
 * @param $order object
 *   The order object
 */
function commerce_recurring_order_initialise_recurring($order) {
  // Wrap the order for easy access to field data.
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  $interval = FALSE;
  if (isset($order_wrapper->field_is_recurring) &&
      isset($order_wrapper->field_recurring_interval)) {

    // Found fields we expect for recurring info.
    
    if ($order_wrapper->field_is_recurring->value()) {
      // We've got an interval
      $interval = $order_wrapper->field_recurring_interval->value();
    }
  }
  
  if (!empty($interval)) {
    $due_dt = new DateObject('now');  // TODO: Prob need to choose date before next delivery. 
    interval_apply_interval($due_dt, $interval[0]);    
    $order->commerce_recurring_next_due[LANGUAGE_NONE][0]['value'] = $due_dt->format('U');
    // Save the updated order.
    commerce_order_save($order);
    
    // Invoke rules event to notify others that the new order has been created
    rules_invoke_all('commerce_recurring_rules_event_initialise_order', $order);
    
    // Log it
    watchdog('Commerce Recurring Order', 'Initialised order @id as new recurring master.', array('@id' => $order->order_id), WATCHDOG_NOTICE);
  }
  
}

/**
 * Condition callback: checks to see if a order has recurring fields set.
 */
function commerce_recurring_order_rules_order_is_recurring($order) {
  // If we actually received a valid order...
  if (!empty($order)) {
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

    if (isset($order_wrapper->field_is_recurring) &&
        isset($order_wrapper->field_recurring_interval)) {

      // Found fields we expect for recurring info.
      
      if ($order_wrapper->field_is_recurring->value()) {
        return TRUE;
      }
    }
  }
  return FALSE;
}

/**
 * Rules action: Update master order with new recurring due date.
 *
 * @param $master_order object
 *   The master order object (first in the sequence)
 */
function commerce_recurring_order_update_master($order) {

  // Wrap the master order for easy access to field data.
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  // Get the due date
  $due_date = $order_wrapper->commerce_recurring_next_due->value();
  
  // Create an object
  $due_date_obj = new DateObject($due_date, date_default_timezone(), 'U');

  $interval = FALSE;
  if (isset($order_wrapper->field_is_recurring) &&
      isset($order_wrapper->field_recurring_interval)) {

    // Found fields we expect for recurring info.
    
    if ($order_wrapper->field_is_recurring->value()) {
      // We've got an interval
      $interval = $order_wrapper->field_recurring_interval->value();
    }
  }
  
  if ($interval && interval_apply_interval($due_date_obj, $interval)) {
    // Update the next due date
    $order->commerce_recurring_next_due[LANGUAGE_NONE][0]['value'] = $due_date_obj->format('U');
    commerce_order_save($order);
  }  
  watchdog('Commerce Recurring Order', 'Updated recurring master @master_id with next due date.',
           array('@master_id' => $order->order_id), WATCHDOG_NOTICE);
}
