This module depends on Drupal Commerce and Commerce Recurring.

Manual steps before creating orders:

1. Add following fields to commerce_order:
- field_is_recurring (Boolean)
- field_recurring_interval (Interval)

 Note: Recommend using commerce_fieldgroup_panes to add those fields to 
 a commerce_order fieldset.

2. Advisable to disable the default rule "Generate Recurring Orders" 
    provided by commerce_recurring. It checks for orders with recurring
    products and assuming you are not using that method, would be extra 
    processing overhead for cron jobs.
    