<?php
/*
 * @file commerce_recurring.admin.inc
 * Provides admin form for commerce_recurring
 * @copyright Copyright(c) 2011 Lee Rowlands
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Lee Rowlands contact at rowlandsgroup dot com
 * 
 */

/**
 * Form builder to display admin form
*/
function commerce_recurring_order_admin_form($form, $form_state) {
  $form = array();

  //return system_settings_form($form);
  return $form;
}